<?php

return [
	'views' => [
		'directory' => 'clean_vc/app/views/foundation/',
	],
	'resources' => [
		'base_dir' => 'dist',
		'dev_dir' => 'resources',

		'frontend' => [
			'js' => [
				'vc-clean-frontend.js'
			],
			'css' => [
				'vc-clean-frontend.css'
			]
		],
		'admin' => [
			'js' => [
				'vc-clean-admin.js'
			],
			'css' => [
				'vc-clean-admin.js'
			]
			
		],
	],

	'vc_ignore_enqueue' => [
		'styles' => [
			'js_composer_front',
			'js_composer_front_custom',
		],
		'scripts' => [
			'wpb_composer_front_js'
		]
	],

	'vc_allowed_shortcodes' => [
		'vc_row',
		'vc_column'
	]


];