<?php

class Config{

	private static $config_file = ABSPATH . 'clean_vc/config/app.php';

	public static function get( $key ){
		return self::readKey( $key );
	}

	public static function getViewsDirectory(){
		$data = include( self::$config_file );
		return $data['views']['directory'];
	}


	/*
		Read a single value from the file
	*/
	private static function readKey( $key ){
		if( ! file_exists( self::$config_file ) ){
			return null;
		}



		$data = include(self::$config_file);

		if( ! isset( $data[$key] ) ){
			return null;
		}

		return $data[$key];
	}
}