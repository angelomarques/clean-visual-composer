<?php

namespace cleanvc\app\Services\General\Controllers\Hooks;

class OverrideVcInit{


	protected $action = 'wp_loaded';
	
	public function __construct(){
		add_action( $this->action, [ $this, 'run' ]);
	}

	public function run(){
		if( vc_manager() ){
			vc_manager()->setCustomUserShortcodesTemplateDir( Config::getViewsDirectory() );
		}
	}
}